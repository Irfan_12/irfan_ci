<?php

class kelas extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_kelas');
	}

	function index()
	{
		echo "index";
	}

	function input()
	{
		$judul="Input Data Kelas";
		$data['judul']="$judul";
		//$this->load->view('tambahKelas',$data,FALSE);
		$this->template->load('media','tambahKelas',$data);
	}

	function edit()
	{
		$id=$this->uri->segment(3);
		$judul="Edit Data Kelas";
		$data['judul']="$judul";
		$data['edit']=$this->M_kelas->getId($id)->row_array();
		//$this->load->view('editKelas',$data,FALSE);
		$this->template->load('media','editKelas',$data);
	}

	function tampil()
	{
		$judul="Tampil Data Kelas";
		$data['judul']="$judul";
		$data['tampil']=$this->M_kelas->tampil()->result();
		//$this->load->view('tampilKelas',$data,FALSE);
		$this->template->load('media','tampilKelas',$data);
	}

	function simpan()
	{
		$data=array
		(
			'kd_kelas'=>$this->input->post('kd_kelas'),
			'nama'=>$this->input->post('nama'),
			'jumlah'=>$this->input->post('jumlah')
		);
		$this->M_kelas->simpan($data);
		redirect('kelas/tampil','refresh');
	}

	function hapus()
	{
		$id=$this->uri->segment(3);
		$this->db->where('kd_kelas',$id);
		$this->db->delete('kelas');
		redirect('kelas/tampil');
	}

	function update()
	{
		$id=$this->input->post('kd_kelas');
		$data=array
		(
			'kd_kelas'=>$this->input->post('kd_kelas'),
			'nama'=>$this->input->post('nama'),
			'jumlah'=>$this->input->post('jumlah')
		);
		$this->M_kelas->update($data,$id);
		redirect('kelas/tampil','refresh');
	}
}