<?php
class Login extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_login');
	}

	function index() //ini untuk menampilkan alamat login
	{
		$judul="Halaman Login";
		$data['judul']=$judul;
		$this->load->view('login');
	}

	function auth()//untuk mengecek Login 
	{
		$username = $this->input->post('username');//inputan
		$password = $this->input->post('password');//inputan
		$cek = $this->M_login->login($username,$password); // dikirim ke model

		if($cek == 1) //jika kondisi benar (1)
		{
			$newdata = array( //pendaftaran session
			'username' => $username,
			'logged_in' => TRUE
			);
			$this->session->set_userdata($newdata);
			redirect ('dashboard');// diarahkan ke halaman dasboard
		}
		else
		{
			redirect('login'); // diarahkan ke halaman login
		}
	}

	function logout()//untuk logout
	{
		session_destroy();// menghapus semua session yang ada
		redirect('login'); //diarahkan ke halaman login
	}


	function sign()
	{
		$data['title']="login";
		$this->load->view('sign',$data);
	}
}