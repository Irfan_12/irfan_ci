<?php
class Tesmedia extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$judul="Tes Media";
		$data['judul']=$judul;
		$this->template->load('media','tesmedia',$data);
	}
}