<?php
class user extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_user');
	}
	function index()
	{
		echo "Index";
	} 

	function simpan()
	{
		$data=array
		(
			'username'=>$this->input->post('username'),
			'password'=>$this->input->post('password'),
			'nama'=>$this->input->post('aktif'),
			'aktif'=>$this->input->post('email'),
			'email'=>$this->input->post('email')
		);
		$this->M_user->simpan($data);
		redirect('siswa/tampil','refresh');
	}


	function input()
	{
		$judul ="Input Data Siswa";
		$data['judul']="$judul";
		//$this->load->view('input_siswa',$data, FALSE);
		$this->template->load('media','input_siswa',$data);
	}

	function edit()
	{
		$id=$this->uri->segment(3);
		$judul ="Edit data Siswa";
		$data['judul']="$judul";
		$data['edit']=$this->M_siswa->getId($id)->row_array();
		//$this->load->view('edit_data',$data, FALSE);
		$this->template->load('media','edit_data',$data);
	}

	function tampil()
	{
		$judul="tampil Data Siswa";//ini adalah judul
		$data['judul']=$judul;//ini adalah variable judul
		$data['tampil']=$this->M_siswa->tampil()->result();
		//$this->load->view('tampil_siswa',$data, FALSE);
		$this->template->load('media','tampil_siswa',$data);
	}

	function update()
	{
		$id=$this->input->post('nim');
		$data=array
		(
			'nim'=>$this->input->post('nim'),
			'nama'=>$this->input->post('nama'),
			'alamat'=>$this->input->post('alamat'),
			'email'=>$this->input->post('email'),

		);
		$this->M_siswa->update($data,$id);
		redirect('siswa/tampil','refresh');
	}

	function hapus()
	{

		$id=$this->uri->segment(3);
		$this->db->where('nim',$id);
		$this->db->delete('siswa');
		redirect('siswa/tampil');
	}


}