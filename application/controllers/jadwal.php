<?php
class Jadwal extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model(array('M_jadwal','M_guru','M_kelas','M_mapel','M_ruang'));
	}

	function index()
	{
		$judul ="Data Jadwal";
		$data['judul']="$judul";
		$data['tampil']=$this->M_jadwal->tampil()->reslut();
		$this->template->load('media','jadwal/tampil',$data);
	}



	function input(){
		$judul="Input Jadwal";
		$data['judul']=$judul;
		$data['nik']=$this->M_guru->tampil()->result();
		$data['kelas']=$this->M_kelas->tampil()->result();
		$data['mapel']=$this->M_mapel->tampil()->result();
		$data['ruang']=$this->M_ruang->tampil()->result();
		$this->template->load('media','jadwal/input',$data); 
	}

	function simpan()
	{
		$data=array
		(
			'nim'=>$this->input->post('nik'),
			'jam'=>$this->input->post('jam'),
			'hari'=>$this->input->post('hari'),
		 	'kd_kelas'=>$this->input->post('kelas'),
		 	'kode_mapel'=>$this->input->post('mapel'),
		 	'kd_ruang'=>$this->input->post(
		 	'ruang')
		 );
		$this->M_jadwal->simpan($data);
		redirect('jadwal/tampil','refresh');

	}

}