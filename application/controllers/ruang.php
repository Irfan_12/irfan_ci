<?php

class ruang extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_ruang');
	}

	function index()
	{
		echo "index";
	}

	function input()
	{
		$judul="Input Data Ruang";
		$data['judul']="$judul";
		//$this->load->view('tambahRuang',$data,FALSE);
		$this->template->load('media','tambahRuang',$data);
	}

	function edit()
	{
		$id=$this->uri->segment(3);
		$judul="Edit Data Ruang";
		$data['judul']="$judul";
		$data['edit']=$this->M_ruang->getId($id)->row_array();
		//$this->load->view('editRuang',$data,FALSE);
		$this->template->load('media','editRuang',$data);
	}

	function tampil()
	{
		$judul="Tampil Data Ruang";
		$data['judul']="$judul";
		$data['tampil']=$this->M_ruang->tampil()->result();
		//$this->load->view('tampilRuang',$data,FALSE);
		$this->template->load('media','tampilRuang',$data);
	}

	function simpan()
	{
		$data=array
		(
			'kd_ruang'=>$this->input->post('kd_ruang'),
			'nama_ruang'=>$this->input->post('nama')
		);
		$this->M_ruang->simpan($data);
		redirect('ruang/tampil','refresh');
	}

	function hapus()
	{
		$id=$this->uri->segment(3);
		$this->db->where('kd_ruang',$id);
		$this->db->delete('ruang');
		redirect('ruang/tampil');
	}

	function update()
	{
		$id=$this->input->post('kd_ruang');
		$data=array
		(
			'kd_ruang'=>$this->input->post('kd_ruang'),
			'nama_ruang'=>$this->input->post('nama')
		);
		$this->M_ruang->update($data,$id);
		redirect('ruang/tampil');
	}
}