<?php

class mapel extends CI_controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_mapel');
	}

	function index()
	{
		echo "index";
	}

	function input()
	{
		$judul="Input Data Mapel";
		$data['judul']="$judul";
		//$this->load->view('tambahMapel',$data,FALSE);
		$this->template->load('media','tambahMapel',$data);
	}

	function edit()
	{
		$id=$this->uri->segment(3);
		$judul="Edit Data Mapel";
		$data['judul']="$judul";
		$data['edit']=$this->M_mapel->getId($id)->row_array();
		//$this->load->view('editMapel',$data,FALSE);
		$this->template->load('media','editMapel',$data);
	}

	function tampil()
	{
		$judul="Tampil Data Mapel";
		$data['judul']="$judul";
		$data['tampil']=$this->M_mapel->tampil()->result();
		//$this->load->view('tampilMapel',$data,FALSE);
		$this->template->load('media','tampilMapel',$data);
	}

	function simpan()
	{
		$data=array
		(
			'kode_mapel'=>$this->input->post('kode_mapel'),
			'nama_mapel'=>$this->input->post('nama_mapel'),
			'sks'=>$this->input->post('sks')
		);
		$this->M_mapel->simpan($data);
		redirect('mapel/tampil','refresh');
	}

	function hapus()
	{
		$id=$this->uri->segment(3);
		$this->db->where('kode_mapel',$id);
		$this->db->delete('mapel');
		redirect('mapel/tampil');
	}

	function update()
	{
		$id=$this->input->post('kode_mapel');
		$data=array
		(
			'kode_mapel'=>$this->input->post('kode_mapel'),
			'nama_mapel'=>$this->input->post('nama_mapel'),
			'sks'=>$this->input->post('sks')
		);
		$this->M_mapel->update($data,$id);
		redirect('mapel/tampil');
	}
}