<?php
class guru extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('M_guru');
	}
	
	function index()
	{
		echo "index";
	}

	function input()
	{
		$judul ="Input Data Guru";
		$data['judul']="$judul";
		//$this->load->view('input_guru',$data, FALSE);
		$this->template->load('media','input_guru',$data);
	}

	function simpan()
	{
		$data=array
		(
			'nim'=>$this->input->post('nik'),
			'nama'=>$this->input->post('nama'),
			'Email'=>$this->input->post('email'),
			'Alamat'=>$this->input->post('alamat'),
		 	'No_Hp'=>$this->input->post('hp'),
		 	'Status'=>$this->input->post('status'),
		 	'Jenis_Kelamin'=>$this->input->post(
		 	'jk'),
		 	'Password'=>$this->input->post('password'),
		 	'Tgl_Lahir'=>$this->input->post('tgl lahir')

		 );
		$this->M_guru->simpan($data);
		redirect('guru/tampil','refresh');

	}

	function edit()
	{
		$id=$this->uri->segment(3);
		$judul ="Edit Data Guru";
		$data['judul']="$judul";
		$data['edit']=$this->M_guru->getId($id)->row_array();
		//$this->load->view('edit_guru',$data, FALSE);
		$this->template->load('media','edit_guru',$data);
	}

	function tampil()
	{

		$judul ="Tampil Data Guru";
		$data['judul']="$judul";
		$data['tampil']=$this->M_guru->tampil()->result();
		//$this->load->view('tampil_guru',$data, FALSE);
		$this->template->load('media','tampil_guru',$data);
	}

	function update()
	{
		$id=$this->input->post('nim');
		$data=array
		(
			'nim'=>$this->input->post('nim'),
			'nama'=>$this->input->post('nama'),
			'Email'=>$this->input->post('email'),
			'Alamat'=>$this->input->post('alamat'),
		 	'No_Hp'=>$this->input->post('hp'),
		 	'Status'=>$this->input->post('status'),
		 	'Jenis_Kelamin'=>$this->input->post(
		 	'jk'),
		 	'Password'=>$this->input->post('password'),
		 	'Tgl_Lahir'=>$this->input->post('tgllahir')
		);
		$this->M_guru->update($data,$id);
		redirect('guru/tampil','refresh');

	}

	function hapus()
	{
		$id=$this->uri->segment(3);
		$this->db->where('nim',$id);
		$this->db->delete('guru');
		redirect('guru/tampil');
	}

}