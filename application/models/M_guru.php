<?php
class M_guru extends CI_Model
{
	function tampil()
	{
		$guru=$this->db->get('guru');
		return $guru;
	}

	function simpan($data)
	{
		$this->db->insert('guru',$data);
	}

	function getId($data)
	{
		$param=array('nim' =>$data);
		return $this->db->get_where('guru',$param);
	}

	function update($data,$id)
	{
		$this->db->where('nim',$id);
		$this->db->update('guru',$data);
	}
}