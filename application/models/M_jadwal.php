<?php
class M_jadwal extends CI_Model
{
	function tampil()
	{
		$jadwal=$this->db->get('jadwal');
		return $jadwal;
	}

	function simpan($data)
	{
		$this->db->insert('jadwal',$data);
	}

	function getId($data)
	{
		$param=array('nim' =>$data);
		return $this->db->get_where('jadwal',$param);
	}

	function update($data,$id)
	{
		$this->db->where('nim',$id);
		$this->db->update('jadwal',$data);
	}
}