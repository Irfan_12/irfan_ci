<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h2><?php echo $judul?></h2>
	<?php echo anchor('kelas/input','Tambah Kelas','class ="btn btn-primary"') ?>
	<table class="table">
		
		<tr>
			<td>
				Kode Kelas
			</td>
			<td>
				Nama Kelas
			</td>
			<td>
				Jumlah
			</td>
			<td>
				Aksi
			</td>
		</tr>

		<?php
		foreach ($tampil as $key => $value)
		{ ?>

		<tr>
			<td>
				<?php echo $value->kd_kelas ?>
			</td>
			<td>
				<?php echo $value->nama ?>
			</td>
			<td>
				<?php echo $value->jumlah ?>
			</td>
			<td>
				<a href="edit/<?php echo $value->kd_kelas ?>" class = "btn btn-info">Edit</a>
				||
				<a href="hapus/<?php echo $value->kd_kelas ?>" class = "btn btn-danger">Hapus</a>
			</td>

		</tr>
	<?php } ?>
	</table>

</body>
</html>