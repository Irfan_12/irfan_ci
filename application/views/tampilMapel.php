<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h2><?php echo $judul?></h2>
	<?php echo anchor('mapel/input','Tambah Mapel','class="btn btn-primary"') ?>
	<table class="table">
		
		<tr>
			<td>
				Kode Mapel
			</td>
			<td>
				Nama Mapel
			</td>
			<td>
				Sks
			</td>
			<td>
				Aksi
			</td>
		</tr>

		<?php
		foreach ($tampil as $key => $value)
		{ ?>

		<tr>
			<td>
				<?php echo $value->kode_mapel ?>
			</td>
			<td>
				<?php echo $value->nama_mapel ?>
			</td>
			<td>
				<?php echo $value->sks ?>
			</td>
			<td>
				<a href="edit/<?php echo $value->kode_mapel ?>" class = "btn btn-info">Edit</a>
				||
				<a href="hapus/<?php echo $value->kode_mapel ?>" class = "btn btn-danger">Hapus</a>
			</td>

		</tr>
	<?php } ?>
	</table>

</body>
</html>