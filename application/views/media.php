<?php
if(empty($this->session->userdata('username')))
{
  redirect('login');
}
else
{

  ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Irfan Al-Aziz</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/bootstrap.min.css">
  <script src="asset/js/jquery.min.js"></script>
  <script src="asset/js/bootstrap.min.js"></script>
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 1500px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;} 
    }
  </style>
</head>
<body>

<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h1><b>Menu Admin</b></h1>
      <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href="<?php echo base_url(); ?>dashboard">Dashboard</a></li>
        <li class="active"><a href="<?php echo base_url(); ?>siswa/tampil">Siswa</a></li>        
        <li class="active"><a href="<?php echo base_url(); ?>guru/tampil">Guru</a></li>
        <li class="active"><a href="<?php echo base_url(); ?>mapel/tampil">Mapel</a></li>
        <li class="active"><a href="<?php echo base_url(); ?>ruang/tampil">Ruang</a></li>
        <li class="active"><a href="<?php echo base_url(); ?>kelas/tampil">Kelas</a></li>
        <li class="active"><a href="<?php echo base_url(); ?>jadwal/tampil">Jadwal</a></li>
        <li class="active"><a href="<?php echo base_url(); ?>login/logout">Log Out</a></li>


      </ul><br>
      
    </div>

    <div class="col-sm-9">
      <br><br>
      
      <h3><b>Judul Artikel</b></h3>
      <hr>
      
      <p><?php echo $contents;?></p>
      

      
      
    </div>
  </div>
</div>

<footer class="container-fluid">
  <p>Footer Text</p>
</footer>

</body>
</html>
<?php } ?>