<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h2><?php echo $judul?></h2>
	<?php echo anchor('ruang/input','Tambah Ruang','class="btn btn-primary"') ?>
	<table class="table">
		
		<tr>
			<td>
				Kode Ruang
			</td>
			<td>
				Nama Ruang
			</td>
			<td>
				Aksi
			</td>
		</tr>

		<?php
		foreach ($tampil as $key => $value)
		{ ?>

		<tr>
			<td>
				<?php echo $value->kd_ruang ?>
			</td>
			<td>
				<?php echo $value->nama_ruang ?>
			</td>
			<td>
				<a href="edit/<?php echo $value->kd_ruang ?>" class = "btn btn-info">Edit</a>
				||
				<a href="hapus/<?php echo $value->kd_ruang ?>" class = "btn btn-danger">Hapus</a>
			</td>

		</tr>
	<?php } ?>
	</table>

</body>
</html>